$(document).ready(function() {
    $("#sort-l").click(function(){
        $("#sel-sort").slideToggle(200);
    });
});
   
$(document).ready(function() {
    $('#cat > div > a').click(function(){
        if ($(this).attr('class') != 'active'){
            $('#cat > div > div').slideUp(400);
            $(this).next().slideToggle(400);
            $('#cat > div > a').removeClass('active');
            $(this).addClass('active');
        }
        else
        {
            $('#cat > div > a').removeClass('active');
            $('#cat > div > div').slideUp(400);
        }
    });
});