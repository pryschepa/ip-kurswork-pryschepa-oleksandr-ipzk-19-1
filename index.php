<?php
	include("include/connect.php");
    $sorting = $_GET["sort"];
    switch ($sorting)
    {
        case 'lth':
        $sorting = 'price_cont ASC';
        break;
        case 'htl':
        $sorting = 'price_cont DESC';
        break;
        default:
        $sorting = 'id_cont DESC';
        break;
    }
?>
<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/header.css" rel="stylesheet" type="text/css">
    <link href="css/footer.css" rel="stylesheet" type="text/css">
    <link href="css/categ.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript" src="/js/shop.js"></script>
	<title>Arber</title>
</head>

<body>
    <div id="body">
        <?php
            include("include/header.php");
        ?>
        <div id="b-um">
            <?php
                include("include/categ.php");
            ?>
        </div>
        <div id="b-cont">
            <div>
                <a id="sort-l">Sorting</a>
                <div id="sel-sort">
                    <a href="index.php?sort=lth">Lowest to highest</a><br />
                    <a href="index.php?sort=htl">Highest to lowest</a>
                </div>
            </div>
            
            <?php
	           $res = mysql_query("SELECT * FROM `t-cont` ORDER BY $sorting", $link);
               if (mysql_num_rows($res) > 0)
               {
                $row = mysql_fetch_array($res);
                do
                {
                    if ($row["img_cont"] != "" && file_exists("./img/".$row["img_cont"]))
                    {
                        $img_path = './img/'.$row["img_cont"];
                        $max_width = 400;
                        $max_height = 400;
                        list($width, $height) = getimagesize($img_path);
                        $ratioh = $max_height/$height;
                        $ratiow = $max_width/$width;
                        $ratio = min($ratioh, $ratiow);
                        $width = intval($ratio*$width);
                        $height = intval($ratio*$height);
                    }
                    else
                    {
                        $img_path = "/img/x.jpg";
                        $width = 169;
                        $height = 169;
                    }
                    
                    echo 
                    '
                    <div id="block-cont">
                        <div class="name-cont">'.$row["name_cont"].'</div> 
                        <div class="img-cont">
                            <img src="'.$img_path.'" width="'.$width.'" height="'.$height.'">
                        </div>
                        <div class="price-cont">Price: '.$row["price_cont"].' UAH</div>
                        <div class="size-cont">Size: '.$row["size_cont"].'</div><br>
                    </div>
                    ';
                }
                while ($row = mysql_fetch_array($res));
               }
            ?>
            
        </div>
        <?php
            include("include/footer.php");
        ?>
    </div>
</body>
</html>