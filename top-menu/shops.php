<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link href="../css/shops.css" rel="stylesheet" type="text/css">
    <link href="../css/header.css" rel="stylesheet" type="text/css">
    <link href="../css/footer.css" rel="stylesheet" type="text/css">
	<title>Arber</title>
</head>

<body>
    <div id="body">
        <?php
            include("../include/header.php");
        ?>
        <div id="b-cont">
            <div class="shop">
                <b>Vinnytsia</b><br><div class="span"></div>
                <u>ARBER</u><br><div class="span"></div>
                st. Pirogova, 13<br><div class="span"></div>
                pirogova.vin@arber.ua<br><div class="span"></div>
                (043) 267-34-54<br><div class="span"></div>
                Opening hours: 09:00 - 20:00
            </div>
            <div class="shop">
                <b>Dnipro</b><br><div class="span"></div>
                <u>ARBER, TC Europe</u><br><div class="span"></div>
                bul. European 1D<br><div class="span"></div>
                europa.dnp@arber.ua<br><div class="span"></div>
                (056) 372-57-12<br><div class="span"></div>
                Opening hours: 09:00 - 20:00
            </div>
            <div class="shop">
                <b>Zhytomyr</b><br><div class="span"></div>
                <u>ARBER, Global Mall</u><br><div class="span"></div>
                st. Kievskaya, 77<br><div class="span"></div>
                global.zhtm@arber.ua<br><div class="span"></div>
                (0412) 41-24-60<br><div class="span"></div>
                Opening hours: 09:00 - 20:00<br>
            </div>
            <div class="shop">
                <b>Kiyv</b><br><div class="span"></div>
                <u>ARBER</u><br><div class="span"></div>
                Khreshchatyk, 29<br><div class="span"></div>
                kreshatik.kie@arber.ua<br><div class="span"></div>
                (067) 354-58-13<br><div class="span"></div>
                Opening hours: 09:00 - 20:00
            </div>
            <div class="shop">
                <b>Uzhhorod</b><br><div class="span"></div>
                <u>ARBER</u><br><div class="span"></div>
                st. Leo Tolstoy, 44<br><div class="span"></div>
                tolstogo.uzh@arber.ua<br><div class="span"></div>
                (031) 261-34-14<br><div class="span"></div>
                Opening hours: 09:00 - 20:00
            </div>
        </div>
        <?php
            include("../include/footer.php");
        ?>
    </div>
</body>
</html>