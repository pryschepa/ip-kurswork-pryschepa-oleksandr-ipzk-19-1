<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link href="../css/help.css" rel="stylesheet" type="text/css">
    <link href="../css/header.css" rel="stylesheet" type="text/css">
    <link href="../css/footer.css" rel="stylesheet" type="text/css">
	<title>Arber</title>
</head>

<body>
    <div id="body">
        <?php
            include("../include/header.php");
        ?>
        <div id="b-cont">
            <h4>CAN I MAKE CHANGES TO THE ORDER?</h4>
            <p>Changes to the order can be made until it is confirmed by the operator. At the time of order confirmation, our services are already starting to work with him.</p>

            <p>If you want to change your order after it has been confirmed by the operator, you will need to cancel your current order and place a new one.</p>
            
            <p>Any changes are possible in payment orders upon receipt.</p>

            <p>In orders paid by bank card on the site, you can only:</p>

            <p>&bull;&ensp;change the address;</p>

            <p>&bull;&ensp;change the type of delivery (courier delivery or self-pickup from the store);</p>

            <p>&bull;&ensp;delete the item or items from the order.</p>

            <p>It is currently not possible to add a position to an order paid for with a bank card on the website.</p>

            <p>If you have such a need, you will need to place a new order for new items or cancel the previous order and place a new one.</p>

            <p>If you have any questions about changes in the order, contact us by phone:</p>

            <p>0800-21-22-14 / 099-978-35-44 / 068-343-86-28 / 093-170-22-23</p>

            <h4>IS FITTING POSSIBLE?</h4>
            <p>When choosing delivery, in most cases, there are three ways to choose:</p>

            <p>&bull;&ensp;<b>Standard</b></p>
            Full redemption without the possibility of fitting in the New Post Office;
 
            <p>&bull;&ensp;<b>Reserve of goods in the store (self-pickup)</b></p>
            Possibility of partial redemption and fitting in the store;
 
            <p>&bull;&ensp;<b>Service</b></p>
            Possibility of the order for any shop with partial redemption and fitting in shop.

            <p>If the selected product is available in the store, you can pick it up in 30 minutes after the online reservation on arber.ua. Alternatively, we can suggest which store has the product you want.</p>

            <p>Shelf life of the collected and deferred order - 3 days.</p>

            <p>When choosing the delivery method "Service", you can try on the ordered products for free and, if something from the ordered did not fit, you can not buy them.
            <br>But remember: for each movement of goods to the store you prepay UAH 50. This amount is not refundable if you cancel the items you have selected.</p>

            <h4>How to order the "Service" delivery service</h4>
            <p>1.&ensp;You need to place an order in the online store, specifying the store where you want to pick up the order.</p>

            <p>2.&ensp;The operator will specify the information on the order, your data and availability of goods in shops.</p>

            <p>3.&ensp;We will issue an invoice for delivery.</p>

            <p>4.&ensp;After the goods arrive at the store, we will call you back.</p>

            <h4>WHAT TO DO IF THE ORDER IS NOT RECEIVED ON TIME?</h4>
            <p>If your order did not arrive on the expected delivery date, contact us by e-mail: ecommerce@arber.ua or by one of the following telephone numbers:</p>

            <p>0800-21-22-14 / 099-978-35-44 / 068-343-86-28 / 093-170-22-23</p>

            <p>We will find out all the information on the order and contact you.</p>

            <h4>THE THING DIDN'T SUIT ME. HOW CAN I GET IT BACK?</h4>
            <p>Arber is probably the only company in our country that uses the most modern customer service technologies.</p>

            <p><b>The principle of omnichannel</b>, implemented specifically to make each of our customers as comfortable as possible, is one of the key steps to providing the perfect service.</p>

            <p>What does this mean for you?</p>

            <p>You can return the product <u>in any</u> convenient store for you online (even if the purchase was made in another).</p>

            <p>If you return the purchase in the same store where you purchased it, the refund is made immediately after registration.</p>

            <p>If you return the goods in any other Arber store, the refund is made to the bank card within 10 days from the date of registration of the return of the goods.</p>

            <p>The return service in any store is available only to members of the Arber League loyalty program.</p>
        </div>
        <?php
            include("../include/footer.php");
        ?>
    </div>
</body>
</html>