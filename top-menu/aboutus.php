<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link href="../css/aboutus.css" rel="stylesheet" type="text/css">
    <link href="../css/header.css" rel="stylesheet" type="text/css">
    <link href="../css/footer.css" rel="stylesheet" type="text/css">
	<title>Arber</title>
</head>

<body>
    <div id="body">
        <?php
            include("../include/header.php");
        ?>
        <div id="b-cont">
            <h4>FOUNDER - A MAN IN LOVE WITH PERFECTION</h4>
            <p>The history of Arber Fashion Group has 30 years of experience and reputation. Its founder is a man in love with perfection.</p>

            <p>Officially, Arber counts down its business biography from the 90s of the XX century, but the unofficial history of the brand began in Odessa, at the same time when its founder, Grigory Arber, hand-sewed the first pair of trousers for the prom.</p>

            <p>Gregory began to work in the studio of individual tailoring, tailor, creating clothes, which were lined up all over Odessa. Gregory became an excellent tailor, able to create the perfect men's suit.</p>

            <p>From the very beginning, he based his uncompromising quality.</p>

            <p>There were no trifles for him in the art of sewing, he was extremely attentive to each path. He was distinguished by an unusual respect for people, emotional sensitivity and delicacy, which he did not change, even becoming the owner of a prosperous company.</p>

            <p>He was one of those who did his job day in and day out, inspiring others, changing the reality around him.</p>

            <p>Now his descendants continue the work of the master, who formed the national industry of business and everyday fashion for men.</p>

            <p>We make a man's style perfect.</p>

            <h4>SEWING WITH THE UKRAINIAN SOUL</h4>
            <p>In 2002, the first garment factory was established in Odessa.</p>

            <p>The first factory was built in accordance with the best world standards, because Gregory Arber toured the world's most modern production. German and Japanese equipment, excellent Italian patterns, high-tech shops, qualified staff - all this created the basis for the highest quality products, which Gregory Arber was not inferior.</p>

            <p>He was demanding and scrupulous in each stage of production, paying special attention to the creation of a standard model (a product on the example of which mass production will be created). At the same time, he willingly shared secrets, passing on invaluable experience to the company's specialists.</p>

            <p>"Quality is always higher than price" - his words, his unchanging principle, the basis of the foundations of his approach to work.</p>

            <p>Today, Arber Fashion Group's own production complex includes four factories that produce a wide range of clothing: jacket and suit group, denim, shirts, knitwear, outerwear.</p>

            <p>The products are made of materials supplied under direct contracts with the world's leading manufacturers.</p>

            <h4>NATIONAL BRAND OF MEN'S CLOTHING</h4>
            <p>Since 2001, the Arber retail chain has been actively developing, opening stores throughout Ukraine. Arber dresses presidents and stars of show business, scientists and workers - every Ukrainian can find their own favorite model.</p>

            <p>By 2004, the network had grown to 30, in 2008 - to 50. In 2015, the chain of branded stores has 100% coverage in Ukraine. In 2007, the Arber brand received the Brand of the Year award in Ukraine.</p>

            <p>Also, since 1996, cooperation with the National Football Team of Ukraine has begun, the company becomes the official supplier of clothing for the Football Federation of Ukraine, and in 2012 Kyiv's Dynamo dresses in Arber suits. Since 2012, Gregory Arber has passed away and Arber continued to develop without its founder, strictly adhering to its principles, while maintaining the highest level of quality.</p>

            <p>The company's 25th anniversary was marked by several important projects, including the opening of the Gregory Arber boutique in a monobrand format - the brand's premium line - dedicated to the founder.</p>

            <p>Today, the brand is represented by 100 Arber stores, which cover 100% of the regions of Ukraine. The chain's stores are represented in the best malls and street retail countries.</p>
            
            <p>"Creating clothes, we create pleasure" - the mission of Arber Fashion Group.</p>

            <p>Day after day, its viability is proved not on paper, but in every path, in every new finished model, in every positive emotion of those who bought perfectly sitting things at an affordable price.</p>

            <p>In 2018, the Arber brand took first place in the prestigious RETAIL AWARDS rating "Consumer Choice" in the category "Men's clothing chain".</p>
        </div>
        <?php
            include("../include/footer.php");
        ?>
    </div>
</body>
</html>