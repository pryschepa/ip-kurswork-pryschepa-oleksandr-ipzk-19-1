<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link href="../css/delivery.css" rel="stylesheet" type="text/css">
    <link href="../css/header.css" rel="stylesheet" type="text/css">
    <link href="../css/footer.css" rel="stylesheet" type="text/css">
	<title>Arber</title>
</head>

<body>
    <div id="body">
        <?php
            include("../include/header.php");
        ?>
        <div id="b-cont">
            <h4>RECEIPT OF ORDERS</h4>
            <p>Phones for communication: (0800) 212-214 (free of charge for all operators of Ukraine), 099 978-35-44, 068 343-86-28, 093 170-22-23.</p>
            <p>Please leave an order through the "Cart".</p>

            <h4>METHODS OF PAYMENT</h4>
            <p>The following payment methods are now available in the ARBER online store:</p>

            <p>&bull;&ensp;Postpay through Nova Poshta delivery service;</p>
            <p>&bull;&ensp;Payment using the service "Payment in installments from PrivatBank";</p>
            <p>&bull;&ensp;Payment by Visa / Mastercard bank card directly on the website (WayForPay service - bank card, Privat24 system, banking terminal);</p>
            <p>&bull;&ensp;Payment in the retail store "ARBER" when choosing the method of delivery "Pickup" when ordering.</p>

            <h4>METHODS OF DELIVERY</h4>
            <p>&bull;&ensp;To the Nova Poshta branch (5 days of storage);</p>
            <p>&bull;&ensp;Delivery by Nova Poshta courier;</p>
            <p>&bull;&ensp;Self-pickup from the ARBER store</p>
            <p>Subject to quarantine and the spread of the COVID-19 virus, we advise all customers to use courier address delivery and pay for contactless orders.
            <br>When ordering with postage, you can open the parcel and view its contents.</p>

            <h4>DELIVERY TIMES</h4>
            <p>Shipment across Ukraine is carried out within 1-4 working days from the moment of processing of the order through Nova Poshta delivery service.</p>

            <p>&bull;&ensp;Delivery times in Kiev and regional centers: 1-3 working days;</p>
            <p>&bull;&ensp;Delivery times to district cities, towns and villages: 2-4 working days;</p>
            <p>&bull;&ensp;Delivery times are approximate.</p>
            <p>Delivery is possible throughout Ukraine except for the Autonomous Republic of Crimea and temporarily occupied territories.</p>

            <h4>SHIPPING COST</h4>
            <p>ARBER sends orders directly from stores, so we offer all customers to pick up orders from our stores.<br>
            When moving goods from one store to another, the customer pays a fixed cost of delivery for each movement 65 UAH. Shipping costs are non-refundable.</p>
            <p>If the goods available in our ARBER store, the product is reserved for 48 hours (the customer does not pay for delivery).</p>
            <p>If there are several items in the order, we cannot rule out that delivery will be from some stores.</p>
            <p>The cost of delivery to the branch of Nova Poshta - is (regardless of the method of payment):</p>

            <p>&bull;&ensp;When sent from the 1st store, the cost of delivery is fixed, in the amount of 65 UAH</p>
            <p>&bull;&ensp;When sent from 2 stores, the cost of delivery is fixed at UAH 80</p>
            <p>&bull;&ensp;When sent from 3 stores, the cost of delivery is fixed at UAH 90</p>
            <p>&bull;&ensp;From 4 or more stores, the cost of delivery from each store is 25 UAH</p>
            <p>When ordering from UAH 800 - delivery to the Nova Poshta branch is FREE.</p>
            <p>If you order more than 2 shipments, one delivery is FREE, the others are paid under the above conditions.</p>
            <p>The order is delivered from several stores only if one.</p>
            <p>The cost of courier delivery is (regardless of the method of payment):</p>

            <p>&bull;&ensp;When sent from the 1st store, the cost of delivery is fixed at UAH 95</p>
            <p>&bull;&ensp;When sent from 2 stores, the cost of delivery is fixed at 130 UAH</p>
            <p>&bull;&ensp;When sent from 3 stores, the cost of delivery is fixed at UAH 150</p>
            <p>&bull;&ensp;From 4 or more stores, the cost of delivery from each store is 40 UAH</p>
            <p>When ordering from UAH 1,500 - courier delivery is FREE.</p>
            <p>If you order more than 2 shipments, one delivery is FREE, the others are paid under the above conditions.</p>
            <p>The order is delivered from several stores only if one.</p>

            <h4>PAY ATTENTION</h4>
            <p>&bull;&ensp;When ordering more than 3 items for a customer with a bad history of purchases is possible only in the format of full subscription or partial subscription of UAH 200 with subsequent payment of the balance upon receipt (for delivery by Nova Poshta - taking into account delivery and cost of service "Receipt of return payment on bank card ").</p>
            <p>&bull;&ensp;Upon receipt of the parcel, check the integrity of the parcel. In case of any problems, inform the operator of the ARBER.ua support service on the day of receipt of the parcel.</p>
            <p>&bull;&ensp;The parcel is stored at the Nova Poshta branch for 5 calendar days (on the 5th day it is automatically returned to the sender).</p>
            <p>&bull;&ensp;In the absence of the buyer or the person authorized to make the purchase, the courier informs by phone about his arrival at the delivery address, waits 15 minutes and leaves the place of delivery, re-informed the customer by phone.</p>
        </div>
        <?php
            include("../include/footer.php");
        ?>
    </div>
</body>
</html>